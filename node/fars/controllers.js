const MDL = require("./models.js");
//const QRY = require("./queries.js");


function toFeatureClass(features) {
	return {
		type: "FeatureCollection",
		features: features
	}
}


exports.get = {
	test: function(req, res, nxt) {
		res.send("here");
	},

	home: function(req, res, nxt) {
		var context = {
			script: [
				"../js/fars.js",
				"../js/ajax.js",
				"../js/hash.js",
				"../lib/js/d3_5.7.0.js",
				"../lib/js/leaflet_1.3.4.js",
				"../lib/js/turf.js",
			],
			style: [
				"../css/fars.css",
				"../css/main.css",
				"../lib/css/leaflet.css",
			],
			title: "FARS",
		};

		res.render("fars", context);

	},
}

function paramsToLst(params) {
	lstStr = "";
	var lst = params.split("|");
	for(var i = 0; i < lst.length; i++) {
		lstStr += "'" + lst[i] + "',";	
	}
	
	return lstStr.slice(0, -1);


}


exports.api = {
	test: function(req, res, nxt) {
		var context = {};

		query = "SELECT now();";


		MDL.select(query, context, () => {
			res.send(context.res);
		})
	},

	accident: {
		state: function(req, res, nxt) {
			var context = {};

			var params = req.params;

			MDL.sql("accident_by_state", (query) => { 

				query = query.split("{{ state }}").join(params.state);

				MDL.select(query, context, () => {
					res.send(toFeatureClass(context.res));
				})
				

			})
		},
		county: function(req, res, nxt) {
			var context = {};

			var params = req.params;

			MDL.sql("accident_by_county", (query) => { 

				query = query.split("{{ state }}").join(params.state);
				query = query.split("{{ county }}").join(params.county);

				MDL.select(query, context, () => {
					res.send(toFeatureClass(context.res));
				})
				

			})
		},
		city: function(req, res, nxt) {
			var context = {};

			var params = req.params;

			MDL.sql("accident_by_city", (query) => { 

				query = query.split("{{ state }}").join(params.state);
				query = query.split("{{ city }}").join(params.city);

				MDL.select(query, context, () => {
					res.send(toFeatureClass(context.res));
				})
				

			})
		},
		radius: function(req, res, nxt) {
			var context = {};

			var params = req.params;

			MDL.sql("accident_by_radius", (query) => { 

				query = query.split("{{ state }}").join(params.state);
				query = query.split("{{ lat }}").join(params.lat);
				query = query.split("{{ lng }}").join(params.lng);
				query = query.split("{{ radius }}").join(params.radius);

				MDL.select(query, context, () => {
					res.send(toFeatureClass(context.res));
				})
				

			})
		},
	},

	vehicle: {
		bodytype: {
			state: function(req, res, nxt) {
				var context = {};
				var params = req.params;


				MDL.sql("vehicle_bodytype_by_state", (query) => { 

					query = query.split("{{ body_type_lst }}").join(paramsToLst(params.body_type_lst));
					query = query.split("{{ state_lst }}").join(paramsToLst(params.state_lst));

					MDL.select(query, context, () => {
						res.send(toFeatureClass(context.res));
					})

				})

			},

			county: function(req, res, nxt) {
				var context = {};
				var params = req.params;


				MDL.sql("vehicle_bodytype_by_county", (query) => { 

					query = query.split("{{ body_type_lst }}").join(paramsToLst(params.body_type_lst));
					query = query.split("{{ state_lst }}").join(paramsToLst(params.state_lst));
					query = query.split("{{ county_lst }}").join(paramsToLst(params.county_lst));


					console.log(query)


					MDL.select(query, context, () => {
						res.send(toFeatureClass(context.res));
					})

				})

			},

		},

		make: {
			state: function(req, res, nxt) {
				var context = {};
				var params = req.params;

				console.log(params)
				MDL.sql("vehicle_make_by_state", (query) => { 

					query = query.split("{{ make_lst }}").join(paramsToLst(params.make_lst));
					query = query.split("{{ state_lst }}").join(paramsToLst(params.state_lst));

					MDL.select(query, context, () => {
						res.send(toFeatureClass(context.res));
					})

				})

			},

			county: function(req, res, nxt) {
				var context = {};
				var params = req.params;


				MDL.sql("vehicle_make_by_county", (query) => { 

					query = query.split("{{ make_lst }}").join(paramsToLst(params.make_lst));
					query = query.split("{{ state_lst }}").join(paramsToLst(params.state_lst));
					query = query.split("{{ county_lst }}").join(paramsToLst(params.county_lst));


					console.log(query)


					MDL.select(query, context, () => {
						res.send(toFeatureClass(context.res));
					})

				})

			},

		},

		accidentId: function(req, res, nxt) {
			var context = {};
			var params = req.params;

			MDL.sql("vehicle_by_accident", (query) => { 

				query = query.split("{{ accident_id }}").join(params.accident_id);

				MDL.select(query, context, () => {
					res.send(context.res);
				})
				

			})
		},
	},

	detail: {
		accidentId: function(req, res, nxt) {
			var context = {};

			var params = req.params;
			MDL.sql("detail_accident", (query) => { 

				query = query.split("{{ accident_id }}").join(params.accident_id);

				MDL.select(query, context, () => {
					res.send(context.res);
				})
				

			})
		},
	},
}
