﻿
select 
	'Feature'::varchar "type", 
	ST_AsGeoJson(geom)::jsonb "geometry", 
	TO_JSONB(props) - 'geom' "properties"
from (
	select *
	from accident
	where state = (
		select distinct state_code
		from fips
		--where state_name = 'OREGON'
		where lower(state_name) = lower('{{ state }}')
	)
) props
WHERE ST_Distance_Sphere(
	geom, st_geomfromtext(
		'POINT({{ lng }} {{ lat }})', 4326
		--'POINT(-122 44)', 4326
	)
) <= {{ radius }} * 1609.34
--100* 1609.34
















