﻿select --﻿SELECT 
	'Feature'::varchar "type", 
	ST_AsGeoJson(geom)::jsonb "geometry", 
	TO_JSONB(props) - 'geom' "properties"
FROM (
	SELECT *
	FROM accident a
	WHERE accident_id IN (
		SELECT accident_id
		FROM person p
		LEFT JOIN vehicle_make b
		ON p.make = b.id
		WHERE lower(b.make) in ( {{ make_lst }} )
		--WHERE lower(b.make) in ( 'honda' )
	)
	AND state::varchar in (
		SELECT distinct state_code::varchar 
			FROM fips
			--WHERE state_name in ( 'OREGON', 'WASHINGTON' )
			WHERE lower(state_name) in ( {{ state_lst }} )
			
	)
) props