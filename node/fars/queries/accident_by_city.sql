﻿SELECT 
	'Feature'::varchar "type", 
	ST_AsGeoJson(geom)::jsonb "geometry", 
	TO_JSONB(props) - 'geom' "properties"
FROM (
	SELECT *
	FROM accident a
	WHERE state::varchar || '-' || city::varchar = (
		SELECT distinct state_code::varchar || '-' || city_code::varchar
		FROM fips
		--WHERE state_name = 'OREGON'
		--AND city_name = 'EUGENE'
		WHERE lower(state_name) = lower('{{ state }}')
		AND lower(city_name) = lower('{{ city }}')
		
	)
) props
