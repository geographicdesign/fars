

SELECT 
	'Feature'::varchar "type", 
	ST_AsGeoJson(geom)::jsonb "geometry", 
	TO_JSONB(props) - 'geom' "properties"
FROM (
	SELECT *
	FROM accident a
	WHERE state::varchar || '-' || county::varchar = (
		SELECT distinct state_code::varchar || '-' || county_code::varchar
		FROM fips
		--WHERE state_name = 'OREGON'
		--AND county_name = 'LANE'
		WHERE lower(state_name) = lower('{{ state }}')
		AND lower(county_name) = lower('{{ county }}')
		
	)
) props
