﻿SELECT 
	'Feature'::varchar "type", 
	ST_AsGeoJson(geom)::jsonb "geometry", 
	TO_JSONB(props) - 'geom' "properties"
FROM (
	SELECT *
	FROM accident a
	WHERE state::varchar = (
		SELECT distinct state_code::varchar 
		FROM fips
		--WHERE state_name = 'OREGON'
		WHERE lower(state_name) = lower('{{ state }}')

	)
) props


