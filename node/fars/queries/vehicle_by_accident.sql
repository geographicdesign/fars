﻿select p.accident_id, p.vehicle_id, p.mod_year, m.make, bt.body_type, bt.body_type_general, 
	sum(case when death_yr > 2000 and death_yr < 3000 then 1 else 0 end)::int death_count,
	count(*)::int occupant_count
from person p
left join vehicle_body_type bt
on p.body_typ = bt.id
left join vehicle_make m
on p.make = m.id
where accident_id = '{{ accident_id }}'
group by p.accident_id, p.vehicle_id, m.make, p.mod_year, bt.body_type, bt.body_type_general


