﻿

select a.*, ad.*
from (	
	select accident_id, array_to_json(array_agg(v)) vehicle_lst
	from(
		select vd.*, p.occupant_lst
		from (
			select p.accident_id, p.veh_no, p.mod_year, m.make, bt.body_type, bt.body_type_general, 
				sum(case when death_yr > 2000 and death_yr < 3000 then 1 else 0 end)::int death_count,
				count(*)::int occupant_count
			from person p
			left join vehicle_body_type bt
			on p.body_typ = bt.id
			left join vehicle_make m
			on p.make = m.id
			where accident_id = '{{ accident_id }}'
			group by p.accident_id,p.veh_no, m.make, p.mod_year, bt.body_type, bt.body_type_general
		) vd
		left join (
			select veh_no, array_to_json(array_agg(pd)) occupant_lst
				from (
					select veh_no, per_no, age, sex, per_typ, inj_sev, seat_pos, rest_use, air_bag, ejection, ej_path, extricat, drinking, alc_det, atst_typ, alc_res, drugs, hospital, doa, lag_hrs
					from person p
					where p.accident_id = '{{ accident_id }}'
				) pd
				group by veh_no
			) p 
		on vd.veh_no = p.veh_no
	) v
	group by accident_id
) ad
left join accident a
on a.accident_id = ad.accident_id
