﻿﻿SELECT 
	'Feature'::varchar "type", 
	ST_AsGeoJson(geom)::jsonb "geometry", 
	TO_JSONB(props) - 'geom' "properties"
FROM (
	SELECT *
	FROM accident a
	WHERE accident_id IN (
		SELECT accident_id
		FROM person p
		LEFT JOIN vehicle_body_type b
		ON p.body_typ = b.id
		WHERE lower(b.body_type_general) in ( {{ body_type_lst }} )
		--WHERE b.body_type_general in ( 'Motorcycle', 'Passenger Car' )
	)
	AND state::varchar in (
			SELECT distinct state_code::varchar 
			FROM fips
			--WHERE state_name in ( 'OREGON', 'WASHINGTON' )
			WHERE lower(state_name) in ( {{ state_lst }} )

	)
) props