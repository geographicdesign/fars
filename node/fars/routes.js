const DIR = "/fars";
const CTRL = require("./controllers");


module.exports = function(app, experess) {
	app.get(DIR + "/test", CTRL.get.test);
	app.get(DIR, CTRL.get.home);

	app.get("/api" + DIR + "/test", CTRL.api.test);
	app.get("/api" + DIR + "/accident/state/:state", CTRL.api.accident.state);
	app.get("/api" + DIR + "/accident/state/:state/county/:county", CTRL.api.accident.county);
	app.get("/api" + DIR + "/accident/state/:state/city/:city", CTRL.api.accident.city);
	app.get("/api" + DIR + "/accident/state/:state/lat/:lat/lng/:lng/radius/:radius", CTRL.api.accident.radius);

	app.get("/api" + DIR + "/vehicle/accident/:accident_id", CTRL.api.vehicle.accidentId);
	app.get("/api" + DIR + "/vehicle/bodytype/:body_type_lst/state/:state_lst", CTRL.api.vehicle.bodytype.state);
	app.get("/api" + DIR + "/vehicle/bodytype/:body_type_lst/state/:state_lst/county/:county_lst", CTRL.api.vehicle.bodytype.county);

	app.get("/api" + DIR + "/vehicle/make/:make_lst/state/:state_lst", CTRL.api.vehicle.make.state);
	app.get("/api" + DIR + "/vehicle/make/:make_lst/state/:state_lst/county/:county_lst", CTRL.api.vehicle.make.county);

	app.get("/api" + DIR + "/detail/accident/:accident_id", CTRL.api.detail.accidentId);

};
