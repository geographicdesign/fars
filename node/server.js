const PORT = 80;

var express = require("express");

var app = express();
var handlebars = require("express-handlebars")
	.create({ defaultLayout: "main" });

var bodyParser = require("body-parser");

app.engine("handlebars", handlebars.engine);
app.set("view engine", "handlebars");


app.use(express.static(__dirname + "/public"));
app.use(bodyParser.urlencoded({ extended: "true" } ));
app.use(bodyParser.json());
app.use(bodyParser.json( { type: "application/vnd.ap+json" } ));


app.get("/help", (req, res, nxt) => { 
	res.send("alive"); 
});


//require("./runOrder/routes")(app, express);
require("./fars/routes")(app, express);

app.listen(PORT, "0.0.0.0", () => {
	console.log("Alive on " + PORT)
})

