﻿
select 
	a.agency, 
	s.site_id, s.site_alias, coalesce(secs, 0) secs,
	case
		when s = 2 then u2/u2
		when s = 3 then u2/u3
		when s = 4 then u2/u4
		when s = 6 then u2/u6
		when s = 7 then u2/u7
		when s = 8 then u2/u8
		when s = 9 then u2/u9
		when s = 10 then u2/u10
		when s = 11 then u2/u11
		when s = 12 then u2/u12
		when s = 1021 then u2/secs
		when s = 1022 then u2/secs
		when s = 1023 then u2/secs
		when s = 1024 then u2/secs
		when s = 1025 then u2/secs
		else 0
	end s2,
	case
		when s = 2 then u3/u2
		when s = 3 then u3/u3
		when s = 4 then u3/u4
		when s = 6 then u3/u6
		when s = 7 then u3/u7
		when s = 8 then u3/u8
		when s = 9 then u3/u9
		when s = 10 then u3/u10
		when s = 11 then u3/u11
		when s = 12 then u3/u12
		when s = 1021 then u3/secs
		when s = 1022 then u3/secs
		when s = 1023 then u3/secs
		when s = 1024 then u3/secs
		when s = 1025 then u3/secs
		else 0
	end s3,
	case
		when s = 2 then u4/u2
		when s = 3 then u4/u3
		when s = 4 then u4/u4
		when s = 6 then u4/u6
		when s = 7 then u4/u7
		when s = 8 then u4/u8
		when s = 9 then u4/u9
		when s = 10 then u4/u10
		when s = 11 then u4/u11
		when s = 12 then u4/u12
		when s = 1021 then u4/secs
		when s = 1022 then u4/secs
		when s = 1023 then u4/secs
		when s = 1024 then u4/secs
		when s = 1025 then u4/secs
		else 0
	end s4,
	case
		when s = 2 then u6/u2
		when s = 3 then u6/u3
		when s = 4 then u6/u4
		when s = 6 then u6/u6
		when s = 7 then u6/u7
		when s = 8 then u6/u8
		when s = 9 then u6/u9
		when s = 10 then u6/u10
		when s = 11 then u6/u11
		when s = 12 then u6/u12
		when s = 1021 then u6/secs
		when s = 1022 then u6/secs
		when s = 1023 then u6/secs
		when s = 1024 then u6/secs
		when s = 1025 then u6/secs
		else 0
	end s6,
	case
		when s = 2 then u7/u2
		when s = 3 then u7/u3
		when s = 4 then u7/u4
		when s = 6 then u7/u6
		when s = 7 then u7/u7
		when s = 8 then u7/u8
		when s = 9 then u7/u9
		when s = 10 then u7/u10
		when s = 11 then u7/u11
		when s = 12 then u7/u12
		when s = 1021 then u7/secs
		when s = 1022 then u7/secs
		when s = 1023 then u7/secs
		when s = 1024 then u7/secs
		when s = 1025 then u7/secs
		else 0
	end s7,
	case
		when s = 2 then u8/u2
		when s = 3 then u8/u3
		when s = 4 then u8/u4
		when s = 6 then u8/u6
		when s = 7 then u8/u7
		when s = 8 then u8/u8
		when s = 9 then u8/u9
		when s = 10 then u8/u10
		when s = 11 then u8/u11
		when s = 12 then u8/u12
		when s = 1021 then u8/secs
		when s = 1022 then u8/secs
		when s = 1023 then u8/secs
		when s = 1024 then u8/secs
		when s = 1025 then u8/secs
		else 0
	end s8,
	case
		when s = 2 then u9/u2
		when s = 3 then u9/u3
		when s = 4 then u9/u4
		when s = 6 then u9/u6
		when s = 7 then u9/u7
		when s = 8 then u9/u8
		when s = 9 then u9/u9
		when s = 10 then u9/u10
		when s = 11 then u9/u11
		when s = 12 then u9/u12
		when s = 1021 then u9/secs
		when s = 1022 then u9/secs
		when s = 1023 then u9/secs
		when s = 1024 then u9/secs
		when s = 1025 then u9/secs
		else 0
	end s9,
	case
		when s = 2 then u10/u2
		when s = 3 then u10/u3
		when s = 4 then u10/u4
		when s = 6 then u10/u6
		when s = 7 then u10/u7
		when s = 8 then u10/u8
		when s = 9 then u10/u9
		when s = 10 then u10/u10
		when s = 11 then u10/u11
		when s = 12 then u10/u12
		when s = 1021 then u10/secs
		when s = 1022 then u10/secs
		when s = 1023 then u10/secs
		when s = 1024 then u10/secs
		when s = 1025 then u10/secs
		else 0
	end s10,
	case
		when s = 2 then u11/u2
		when s = 3 then u11/u3
		when s = 4 then u11/u4
		when s = 6 then u11/u6
		when s = 7 then u11/u7
		when s = 8 then u11/u8
		when s = 9 then u11/u9
		when s = 10 then u11/u10
		when s = 11 then u11/u11
		when s = 12 then u11/u12
		when s = 1021 then u11/secs
		when s = 1022 then u11/secs
		when s = 1023 then u11/secs
		when s = 1024 then u11/secs
		when s = 1025 then u11/secs
		else 0
	end s11,
	case
		when s = 2 then u12/u2
		when s = 3 then u12/u3
		when s = 4 then u12/u4
		when s = 6 then u12/u6
		when s = 7 then u12/u7
		when s = 8 then u12/u8
		when s = 9 then u12/u9
		when s = 10 then u12/u10
		when s = 11 then u12/u11
		when s = 12 then u12/u12
		when s = 1021 then u12/secs
		when s = 1022 then u12/secs
		when s = 1023 then u12/secs
		when s = 1024 then u12/secs
		when s = 1025 then u12/secs
		else 0
	end s12
from (
	select site_id, site_alias
	from pm.rftrunkingsite ts
	union all
	select site_id, site_alias
	from pm.consolesite cs
)s
left join (
	select agency, air_site s, sum(air_secs) secs, 
		sum(used_02) u2, sum(used_03) u3, sum(used_04) u4, 
		sum(used_06) u5, sum(used_06) u6, sum(used_07) u7, 
		sum(used_08) u8, sum(used_09) u9, sum(used_10) u10, 
		sum(used_11) u11, sum(used_12) u12
	from genesis.airtime a
	left join pm.talkgroup tg
	on a.talkgroup_id = tg.talkgroup_id
	--{{ where }}
	group by agency, air_site
) a
on s.site_id = a.s

where a.agency is not null
order by s.site_id;











