const MDL = require("./models.js");
const QRY = require("./queries.js");


exports.get = {
	test: function(req, res, nxt) {
		res.send("here");
	},
}
exports.api = {
	test: function(req, res, nxt) {
		var context = {};

		var params = { "agency": "EPD" }

		var where = "WHERE "

		for (param in params) {
			where += param + "::varchar = '" + params[param] + "' and ";
		}
		
		where = where.slice(0, -5);
		console.log(where)

		QRY.fetch("airtime", (query) => { 

			query = query.split("--{{ where }}").join(where);

			console.log(query)

			MDL.select(query, context, () => {
				res.send(context.res);
			})
			

		})

	},
/*
	agencyMonthTopNSecs: function(req, res, nxt) {
		var context = {};
		var params = req.params;

		var query = `SELECT *
			FROM (
				SELECT ROW_NUMBER() OVER (ORDER BY agency_secs desc) rank,
					agency_name, agency_secs, 
					agency_secs / all_secs * 100.0 system_use
				FROM (
					SELECT DISTINCT agency_name, sum(total_secs) OVER 
						(PARTITION BY agency_name) agency_secs,
						SUM(total_secs) OVER () all_secs
					FROM airtime.sw7
					WHERE EXTRACT(MONTH FROM dt) = {{month}}
					AND EXTRACT(YEAR FROM dt) = {{year}}
				) a
			) r
			WHERE r.rank < {{n}} + 1; `

		query = query.split("{{month}}").join(params.month)
		query = query.split("{{year}}").join(params.year)
		query = query.split("{{n}}").join(params.n)


		MDL.select(query, context, () => {
			res.send(context.res);
		})
	}, 

	specAgencyMonthTopNSecs: function(req, res, nxt) {
		var context = {};
		var params = req.params;

		var query = `SELECT *
			FROM (
				SELECT ROW_NUMBER() OVER (ORDER BY agency_secs desc) rank,
					agency_name, agency_secs, 
					agency_secs / all_secs * 100.0 system_use
				FROM (
					SELECT DISTINCT agency_name, sum(total_secs) OVER 
						(PARTITION BY agency_name) agency_secs,
						SUM(total_secs) OVER () all_secs
					FROM (
						SELECT agency_name, total_secs
						FROM airtime.sw7
						WHERE EXTRACT(MONTH FROM dt) = {{month}}
						AND EXTRACT(YEAR FROM dt) = {{year}}
						AND agency_name != 'ESF'
						UNION ALL
						SELECT '*ESF', total_secs
						FROM airtime.fire
						WHERE EXTRACT(MONTH FROM dt) = {{month}}
						AND EXTRACT(YEAR FROM dt) = {{year}}
					) d
				) a
			) r
			WHERE r.rank < {{n}} + 1; `

		query = query.split("{{month}}").join(params.month)
		query = query.split("{{year}}").join(params.year)
		query = query.split("{{n}}").join(params.n)


		MDL.select(query, context, () => {
			res.send(context.res);
		})
	}, 

	agencyMonthTopNPtts: function(req, res, nxt) {
		var context = {};
		var params = req.params;

		var query = `SELECT *
			FROM (
				SELECT ROW_NUMBER() OVER (ORDER BY agency_ptts desc) rank,
					agency_name, agency_ptts, 
					(agency_ptts * 100.0) / all_ptts system_use
				FROM (
					SELECT DISTINCT agency_name, sum(total_ptts) OVER 
						(PARTITION BY agency_name) agency_ptts,
						SUM(total_ptts) OVER () all_ptts
					FROM airtime.sw7
					WHERE EXTRACT(MONTH FROM dt) = {{month}}
					AND EXTRACT(YEAR FROM dt) = {{year}}
				) a
			) r
			WHERE r.rank < {{n}} + 1; `

		query = query.split("{{month}}").join(params.month)
		query = query.split("{{year}}").join(params.year)
		query = query.split("{{n}}").join(params.n)

		MDL.select(query, context, () => {
			res.send(context.res);
		})
	}, 

	specAgencyMonthTopNPtts: function(req, res, nxt) {
		var context = {};
		var params = req.params;

		var query = `SELECT *
			FROM (
				SELECT ROW_NUMBER() OVER (ORDER BY agency_ptts desc) rank,
					agency_name, agency_ptts, 
					(agency_ptts * 100.0) / all_ptts system_use
				FROM (
					SELECT DISTINCT agency_name, sum(total_ptts) OVER 
						(PARTITION BY agency_name) agency_ptts,
						SUM(total_ptts) OVER () all_ptts
					FROM (
						SELECT agency_name, total_ptts
						FROM airtime.sw7
						WHERE EXTRACT(MONTH FROM dt) = {{month}}
						AND EXTRACT(YEAR FROM dt) = {{year}}
						AND agency_name != 'ESF'
						UNION ALL
						SELECT '*ESF', total_ptts
						FROM airtime.fire
						WHERE EXTRACT(MONTH FROM dt) = {{month}}
						AND EXTRACT(YEAR FROM dt) = {{year}}
					) d

				) a
			) r
			WHERE r.rank < {{n}} + 1; `

		query = query.split("{{month}}").join(params.month)
		query = query.split("{{year}}").join(params.year)
		query = query.split("{{n}}").join(params.n)

		MDL.select(query, context, () => {
			res.send(context.res);
		})
	}, 
*/
}
