document.addEventListener("DOMContentLoaded", onload);

const token = "pk.eyJ1IjoicnVkZWRvZ2cxODciLCJhIjoiY2pmOTVlY3M3MHA0ejJ5b2I4dnB6eGhkaSJ9.ZAAaN8BhLBKs1-ac4_jBfQ";
var map;
var mapSvg;
var path;
var defaultZoom = 12;
var table;
var addrPoints;

function addMap(container) {
	container = container.replace("#", "");
	map = L.map(container)
		.setView([44.063, -123.049], defaultZoom);
/*
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
	    	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	    	maxZoom: 18,
	    	id: 'mapbox.streets',
	    	accessToken: token
	}).addTo(map); 
*/
	L.tileLayer('https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
		attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
		maxZoom: 16
	}).addTo(map);


	L.svg({clickable:true}).addTo(map);

}


function addTable(container) {
	table = d3.select(container)
		.append("table");

	table.append("thead")
		.append("tr");

	table.append("tbody");
}


function updateTableHead(features) {
	console.log("updateTableHead(features);");
	
	var thead = table.select("thead")
		.select("tr");

	if(features.length > 0) {
		headers = Object.keys(features[0].properties);
	} else {
		headers = [];
	}

	var hr = thead.selectAll("th")
		.data(headers, (d) => { return d; });

	hr.enter()
		.append("th")
		.merge(hr)
		.html( (d) => { return d; });

	hr.exit()
		.remove();

	return headers;
}


function updateTableBody(headers, features) {
	console.log("updateTableBody(features);");
	
	var tbody = table.select("tbody");

	var data = function(d) {
		var r = [];
		for(var i = 0; i < headers.length; i++) {
			r[i] = d.properties[headers[i]];
		}
		return r;
	}

	var handleEvent = function(d) {
		d3.event.preventDefault();
		var tgt = d3.select("#gid_" + d.properties.gid);

		d3.selectAll(".cad-shp")
			.attr("stroke", "grey")
			.attr("stroke-width", 1)

		tgt.attr("stroke", "yellow")
			.attr("stroke-width", 3)


		var feat = features.filter( (f) => {
			if(f.properties.gid == d.properties.gid) { return f; }
		})[0];

		var center = turf.center(feat).geometry.coordinates;
		map.setView([center[1], center[0]], 12);
		console.log(feat)
		
	}

	var row = tbody.selectAll("tr")
		.data(features)


	row.enter()
		.append("tr")
		.merge(row)
		.on("mouseover", handleEvent)
		.selectAll("td")
		.data(data)
		.enter()
		.append("td")
		.html( (d) => { return d; });


	row.exit()
		.remove();

	
	var cells = row.selectAll("td")
		.data(data)
		.html( (d) => { return d; })

	cells.enter()
		.html( (d) => { return d; })

	cells.exit()
		.remove();
	
		
}


function projectPoint(x, y) {
	var point = map.latLngToLayerPoint(new L.LatLng(y, x));
	this.stream.point(point.x, point.y);

}

function createPath() {
	var transform = d3.geoTransform({ point: projectPoint });
	path = d3.geoPath()
		.projection(transform);

}


function getAddrPoints(build) {
	addrPoints = null;
	ajax.get("/api/mapupdate/addr_points/build/b_" + build, (featureCollection) => {
		addrPoints = featureCollection;
	})
}


function getFeatureData(g) {
	ajax.get("/api/mapupdate/fire_gp/compare/b_20180911/b_20161005", (featureCollection) => {
		var features = featureCollection.features;

		center = turf.center(featureCollection).geometry.coordinates;
		map.setView([center[1], center[0]], 12);

		var feats = g.selectAll("path")
			.data(features)
			.enter()
			.append("path")
			.style("pointer-events", "auto")
			.style("stroke", "red")
			.style("stroke-width", 2)
			.attr("stroke-opacity", (d) => {
				//if(d.properties.cad.substring(0, 2) == 'OU') { return .2; }
				return 1;
			})
			.attr("fill", "red")
			.attr("fill-opacity", (d) => {
				//if(d.properties.cad.substring(0, 2) == 'OU') { return .2; }
				return .75;
			})
			.on("click", function(d) {
		//		center = turf.center(d).geometry.coordinates;
		//		map.setView([center[1], center[0]], 12);
				//

				console.log(d.properties);
			});

		
		map.on("moveend", update);

		update();

		function update() {
			feats.attr("d", path)
		}

	});
}


var hashCallback = function() {	
	var len = hash.params.length;
	
	switch(len) {
		case 1: 
			if(hash.params[0] == "layers") {
				layersHandler();

			}
			break;

		case 2:
			if(hash.params[1] == "builds") { 
				layerBuildsHandler(hash.params[0]);
			}
			break;

		case 3: 
			if(hash.params[1] == "build") {
				layerBuildHandler(hash.params[0], hash.params[2]);
			}
			break;

		case 4:
			if(hash.params[1] == "compare") {
				layerCompareHandler(hash.params[0], hash.params[2], hash.params[3]);
			}

		default:
			break;

	}


}

function layersHandler() {
	console.log("in layersHandler()");
	ajax.get("api/mapupdate/layers", (data) => {
		console.log("layers", data)
	})
}

function layerBuildsHandler(layer) {
	console.log("in layerBuildsHandler(" + layer + ")");
	ajax.get("api/mapupdate/" + layer + "/builds", (data) => {
		console.log("layers", data)
	})

}

function layerBuildHandler(layer, build) {
	console.log("in layerBuildHandler(" + layer + ", " + build + ")");

	var g = mapSvg.select("g")
		.attr("class", "leaflet-zoom-hide")
		.style("pointer-events", "auto");

	ajax.get("api/mapupdate/" + layer + "/build/b_" + build, (featureCollection) => {

		var feats = g.selectAll("path")
			.data(featureCollection.features);

		console.log(feats);


		feats.enter()
			.append("path")
			.attr("stroke", "grey")
			.attr("fill", "green")
			.attr("fill-opacity", .5)
			.style("pointer-events", "auto")
			.merge(feats)
			.attr("d", path)
			.on("click", (d) => {
				console.log(d.properties)
			})


		feats.exit()
			.remove()


		map.on("moveend", update);

		function update() {
			console.log("moveend")
			g.selectAll("path")
				.attr("d", path)
		}


	})

}

function layerCompareHandler(layer, buildA, buildB) {
	console.log("in layerCompareHandler(" + layer + ", " + buildA + ", " + buildB + ")");

	var mapG = mapSvg.select("g")
		.attr("class", "leaflet-zoom-hide")
		.style("pointer-events", "auto");


	ajax.get("api/mapupdate/" + layer + "/compare/b_" + buildA + "/b_" + buildB, (featureCollection) => {
		var headers = updateTableHead(featureCollection.features);


		updateTableBody(headers, featureCollection.features);
	
		var feats = mapG.selectAll("path")
			.data(featureCollection.features);

		feats.enter()
			.append("path")
			.attr("id", (d) => {
				return "gid_" + d.properties.gid;
			})
			.attr("class", "cad-shp")
			.attr("stroke", "grey")
			.attr("stroke-width", 1)
			.attr("fill", (d) => {
				if(d.properties.to_value.substring(0, 2) == "OU" && d.properties.from_value.substring(0, 2) != "OU") {
					return "red";
				}
				return 	"green"
			})
			.attr("fill-opacity", .5)
			.style("pointer-events", "auto")
			.merge(feats)
			.attr("d", path)
			.on("click", (d) => {
				console.log(d.properties)
				getAddressInPolygon(d.geometry, hash.params[3]);

			})


		feats.exit()
			.remove()


		map.on("moveend", update);

		function update() {
			mapG.selectAll("path")
				.attr("d", path)
		}


	})

}


function getAddressInPolygon(features, build) {
	var url = "/api/mapupdate/addr_points/in_polygon";
	payload = { 
		features: features,
		build: build
	};

	ajax.post(url, payload, (featureCollection) => {
		var addresses = featureCollection.features;
		var addrLst = ''
		for(var i = 0; i < addresses.length; i++) {
			var props = addresses[i].properties;
			addrLst += props.concat_add + ", " + props.city_name + "\n";

		}

		alert((addrLst));

	});
}

function onload() {
	hash.callback = hashCallback;

	var body = d3.select("body");
	var mapDiv = d3.select("#mapid")
		.style("height", "480px");

	var tableDiv = d3.select("#table_div")
		.style("height", "480px");

	addMap("#mapid");

	addTable("#table_div");

	mapSvg = d3.select("#mapid")
		.select("svg")
		.style("pointer-events", "auto");

	d3.select(".leaflet-overlay-pane")
		.style("pointer-events", "auto");

	createPath();




	
};
