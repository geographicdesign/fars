document.addEventListener("DOMContentLoaded", onload);

const token = "pk.eyJ1IjoicnVkZWRvZ2cxODciLCJhIjoiY2pmOTVlY3M3MHA0ejJ5b2I4dnB6eGhkaSJ9.ZAAaN8BhLBKs1-ac4_jBfQ";

var LAT = 43.22;
var LNG = -123.34;
var RAD = 50;

var accidents;
var map;
var mapSvg;
var mapG;
var transform;
var path;
var url;

function projectPoint(x, y) {
	var point = map.latLngToLayerPoint(new L.LatLng(y, x));
	this.stream.point(point.x, point.y);
}

function getBBox(features) {
	var r = {};
	var fc = {
		type: "FeatureCollection",
		features: features.filter( (d) => {
			if(
				d.properties.latitude != 0 &&
				d.properties.latitude != 99.9999 &&
				d.properties.latitude != 88.8888 &&
				d.properties.latitude != 77.7777 &&
				d.properties.longitud != 0 &&
				d.properties.longitud != 999.9999 &&
				d.properties.longitud != 888.8888 &&
				d.properties.longitud != 777.7777
			) {
				return d;
			}
		})
	};

	
	r.bbox = turf.bbox(fc)
	r.center = turf.center(fc).geometry.coordinates;

	
	var z = turf.distance(
		turf.point([r.bbox[0], r.bbox[1]]),
		turf.point([r.bbox[2], r.bbox[1]]),
		{ units: "miles" }
	)

	console.log(r)
	//alert(z)

	return r;
}


function addMap(container) {

	map = L.map(container.substr(1))
		.setView([LAT, LNG], 13);
/*
	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
	    	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	    	maxZoom: 18,
	    	id: 'mapbox.streets',
	    	accessToken: token
	}).addTo(map);
*/

L.tileLayer('https://cartodb-basemaps-{s}.global.ssl.fastly.net/dark_all/{z}/{x}/{y}{r}.png', {
	attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="http://cartodb.com/attributions">CartoDB</a>',
	subdomains: 'abcd',
	maxZoom: 19
}).addTo(map);

	mapSvg = d3.select(map.getPanes().overlayPane)
		.append("svg")
		.style("pointer-events", "auto");

	d3.select(".leaflet-overlay-pane")
		.style("pointer-events", "auto");

	mapG = mapSvg.append("g")
		.attr("class", "leaflet-zoom-hide");
}


function testData(url) {
	if(url == null) { return };

	ajax.get(url, (featureCollection) => {

		accidents = featureCollection.features;
		features = accidents;

		if(hash.query != null) {
			features = features.filter( (d) => {
				var i = 0;
				var j = 0
				for(key in hash.query) {
					val = hash.query[key];
					if(d.properties[key] == val) { i++ }
					j++;
				}
				if(j == i) { return d; }
			})
		}	
		
		var r = getBBox(features);
		map.setView([r.center[1], r.center[0]], 7);

		var point = mapG.selectAll("circle")
			.data(features);

		point.enter()
			.append("circle")
			.attr("id", (d) => { return "point-" + d.properties.accident_id; })
			.attr("class", "map-transform crash-point")
			.attr("r", 4)
			.attr("fill-opacity", .5)
			.attr("fill", "red")
			.style("pointer-events", "auto")
			.merge(point)
			.on("click", function(d) {
				var accidentId = d.properties.accident_id;
				var detUrl = "/api/fars/detail/accident/{{ accident_id }}"
					.replace("{{ accident_id }}", accidentId);

				ajax.get(detUrl, (details) => {
					console.log(details[0]);

				})
			})
			.on("mouseenter", (d) => {
				d3.select("#popup-" + d.properties.accident_id)
					.style("display", "block")

				d3.select("#popup-text-" + d.properties.accident_id)
					.style("display", "block")
			})
			.on("mouseout", (d) => {
				d3.select("#popup-" + d.properties.accident_id)
					.style("display", "none")
j
				d3.select("#popup-text-" + d.properties.accident_id)
					.style("display", "none")
			})

		point.exit()
			.remove()

		var popupG = mapG.selectAll(".popup-g")
			.data(features);

		var popupEnter = popupG.enter()
			.append("g")
			.attr("class", "map-transform popup-g")
			.merge(popupG);
	
		var popupRect = popupEnter.append("rect")
			.attr("id", (d) => { return "popup-" + d.properties.accident_id; })
			.attr("class", "popup")
			.attr("width", 150)
			.attr("height", 200)


		var popupText = popupEnter.append("text")
			.attr("id", (d) => { return "popup-text-" + d.properties.accident_id; })
			.attr("class", "popup-text")
			.text("xxx")

		popupG.exit()
			.remove()

		map.on("viewreset", reset);
		map.on("moveend", reset);
		reset();

		function reset() {
			var bounds = path.bounds(featureCollection);
			var topLeft = bounds[0];
			var bottomRight = bounds[1];

			mapSvg.attr("width", 1000000)//bottomRight[0] - topLeft[0])
				.attr("height", 1000000)//bottomRight[1] - topLeft[1])

			mapG.selectAll(".map-transform")
				.attr("transform", (d) => {
				var y = d.geometry.coordinates[1];
				var x = d.geometry.coordinates[0];
				return "translate(" + 
					map.latLngToLayerPoint(new L.LatLng(y, x)).x + "," +
					map.latLngToLayerPoint(new L.LatLng(y, x)).y + ")";

			})
		}

	});
}


function hashCallback() {
	//var url = "/api/fars/accident/state/oregon/county/lane"
	var url = "/api/fars/" + hash.params.join("/");
	
	testData(url);

}

function onload() {
	hash.callback = hashCallback;
	d3.select("html")
		.style("height", "100%");


	var body = d3.select("body")
		.style("height", "100%");

	var mapDiv = d3.select("#mapid")
		.style("height", "100%")
		.style("cursor", "default");

	addMap("#mapid");

	transform = d3.geoTransform({ point: projectPoint });

	path = d3.geoPath()
		.projection(transform);

	testData(null);

}



function onload2() {
	var body = d3.select("body");

	body.transition()
		.duration(5000)
		.style("background-color", "#FFFFFF");

	var mapDiv = d3.select("#mapid")
		.style("height", "480px");

	addMap("#mapid");
	var map = L.map("mapid")
		.setView([LAT, LNG], 13);

	L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
	    	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	    	maxZoom: 18,
	    	id: 'mapbox.streets',
	    	accessToken: token
	}).addTo(map);

	var svg = d3.select(map.getPanes().overlayPane)
		.append("svg")
		.style("pointer-events", "auto");

	d3.select(".leaflet-overlay-pane")
		.style("pointer-events", "auto");


	map.on("click", (e) => {
		var latLng = map.mouseEventToLatLng(e.originalEvent);	
		console.log(latLng)

	})


	var g = svg.append("g")
		.attr("class", "leaflet-zoom-hide");

	var transform = d3.geoTransform({ point: projectPoint });

	var path = d3.geoPath()
		.projection(transform);


	var url = "/api/fars/accident/state/oregon/lat/{{ lat }}/lng/{{ lng }}/radius/{{ rad }}"
	var url = "/api/fars/accident/state/oregon/county/lane"
		.replace("{{ lat }}", LAT)
		.replace("{{ lng }}", LNG)
		.replace("{{ rad }}", RAD);

	ajax.get(url, (featureCollection) => {

		accidents = featureCollection.features;
		features = accidents;

	
		if(hash.query != null) {
			features = features.filter( (d) => {
				var i = 0;
				var j = 0
				for(key in hash.query) {
					val = hash.query[key];
					console.log(key, '->', val);
					console.log(d.properties[key]);
					if(d.properties[key] == val) {
						i++;
					}
					j++;
					
				
				}
				if(j == i) {
					return d;
				}	
			})
		}	
		
		var r = getBBox(features);
		map.setView([r.center[1], r.center[0]], 7);

		var feats = g.selectAll("path")
			.data(features)
			.enter()
			.append("path")
			.style("pointer-events", "auto")
			.on("click", function(d) {
				var accidentId = d.properties.accident_id;
				var url = "/api/fars/detail/accident/{{ accident_id }}"
					.replace("{{ accident_id }}", accidentId);

				ajax.get(url, (details) => {
					console.log(details[0]);

				})
			})

		map.on("viewreset", reset);
		map.on("moveend", reset);
		reset();

		function reset() {
			var bounds = path.bounds(featureCollection);
			var topLeft = bounds[0];
			var bottomRight = bounds[1];

			svg.attr("width", bottomRight[0] - topLeft[0])
				.attr("height", bottomRight[1] - topLeft[1])
				.style("left", topLeft[0] + "px")
				.style("top", topLeft[1] + "px");

			g.attr("transform", "translate(" +
				-topLeft[0] + "," +
				-topLeft[1] + ")"
			);

			feats.attr("d", path)
				.attr("fill-opacity", .2)
				.attr("stroke", "red")

		}

	});

	function projectPoint(x, y) {
		var point = map.latLngToLayerPoint(new L.LatLng(y, x));
		this.stream.point(point.x, point.y);

	}
	
};
