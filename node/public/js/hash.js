var hash = {
	params: [],
	query: null,
	callback: null,
	fetch: function() {
		var hashStr = location.hash.substring(1);

		var p = hashStr.split("?")[0];
		var q = hashStr.split("?")[1];

		hash.params = [];
		hash.query = null;

		if(p) {
			hash.params = p.split("/");
		}

		if(q) {
			hash.query = {};
			qLst = q.split("&");
			for(var i = 0; i < qLst.length; i++) {
				var keyVal = qLst[i].split("=");
				key = keyVal[0];
				val = keyVal[1];
				if(val) {
					hash.query[key] = val;
				}

			}
		}

		console.log("hash.params: ", hash.params);
		console.log("hash.query: ", hash.query);

		if(typeof hash.callback == "function") {
			hash.callback();

		}


	}


}

window.addEventListener("hashchange", hash.fetch);
window.addEventListener("DOMContentLoaded", hash.fetch);

