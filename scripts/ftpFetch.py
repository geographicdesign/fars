import ftplib
import os
import shutil
import zipfile
from dbfpy import dbf
from dbfpy import fields
import psycopg2

STATE = None

host = "localhost"
#host = "50.112.20.42"
db = "fars"
user = "postgres"
pwd = "RooCoon051708"

conn_str = "host='{}' dbname='{}' user='{}' password='{}'".format(host, db, user, pwd)

conn = psycopg2.connect(conn_str)
conn.autocommit = True
cursor = conn.cursor()

ftpUrl = "ftp.nhtsa.dot.gov"
root = "fars"
temp = "../temp"

def psqlExecute(query):
	try:
		cursor.execute(query)
		return 1
	except:
		print query
		print
		print
		return 0

def psqlSelect(query):
	try:
		cursor.execute(query)
		return cursor.fetchall()

	except:
		print query
		print
		print
		return 0

def ftpConnect(url, root):
	ftp = ftplib.FTP(url)
	ftp.login()
	ftp.cwd(root)

	return ftp

def fetchData(ftp, f):
	print "\tfetching {}{}".format(f["ftpDir"], f["tgt"])
	ftp.cwd(f["ftpDir"])
	if ftp.nlst().index(f["tgt"]) >= 0:
		ftp.retrbinary("RETR " + f["tgt"], open(f["saveTo"], "wb").write)
	else:
		print "\tERROR {} not in FTP dirctory".format(tgt)
		
	ftp.cwd(f["ftpBack"])
		

def unzipData(f):
	print "\textracting {}".format(f["saveTo"])

	if len(os.listdir(f["saveTo"].replace(".zip", ""))) > 0:
		return os.listdir(f["saveTo"].replace(".zip", ""))[0][-3:]

	z = zipfile.ZipFile(f["saveTo"])
	
	makeDir(f["dbfDir"])
	dataType = None

	for d in z.namelist():
		if d[-3:].lower() == "dbf" or d[-3:].lower() == "csv":
			z.extract(d, f["dbfDir"])
			dataType = d[-3:]

	path = f["saveTo"].replace(".zip", "")

	for f in os.listdir(path):
		f = "{}/{}".format(path, f)
		os.rename(f, f.lower())

	return dataType


def getCsvDataType(key, val, dataTypes):
	if "." in val:
		try: 
			float(val)
			dataTypes[key]["float"] += 1
			return
		except:
			dataTypes[key]["char"] += 1
			return 
	try:
		int(val)
		dataTypes[key]["int"] += 1
		return 
	except:
		dataTypes[key]["char"] += 1
		return 


def getCsvData(headFields, csvFile):
	csvData = []
	#dataTypes = {}
	#for h in headFields:
	#	dataTypes[h] = {"int": 0, "float": 0, "char": 0}

		
	with open(csvFile, "rb") as f:
		data = f.read().split("\r\n")


	for i in range(0, len(data)):
		row = data[i].split(",")
		if i == 0:
			head = row

		rowData = {}
		if i > 0 and len(row) > 1:
			for i in range(0, len(row)):
				key = head[i]
#				if key in headFields:
				val = row[i]
				rowData[key.lower()] = val
					
				#getCsvDataType(key, val, dataTypes)

		if len(rowData) > 0:
			csvData.append(rowData)

	'''
	for d in dataTypes:
		if dataTypes[d]["int"] == len(csvData):
			dataTypes[d] = "int"

		elif dataTypes[d]["float"] == len(csvData):
			dataTypes[d] = "float"
		
		else:
			dataTypes[d] = "char"

	return csvData, dataTypes
	'''
	return csvData

def getDbfHeaderLst(headFields, dbfFile):
	headerLst = []
	for head in dbfFile.header.fields:
		h = str(head).split("\n")[0]
		h = h.split(" ");
		h[:] = [x for x in h if x != ""]
		if h[0] in headFields:
			headerLst.append({
				"field": h[0],
				"type": h[1],
				"size": h[2],
				"prec": h[3]
			})

	return headerLst;


def getDbfHeaderLst2(dbfFile):
	headerLst = []
	for head in dbfFile.header.fields:
		h = str(head).split("\n")[0]
		h = h.split(" ");
		h[:] = [x for x in h if x != ""]
		headerLst.append( h[0] )

	return headerLst;


def getCsvHeaderLst2(csvFile):
	with open(csvFile, "rb") as f:
		headerLst = f.read().split("\n")[0].replace("\r", "").split(",")
		

	return headerLst


def insertDbfRowData(table, headerLst, rowDict):
	keys = "\t{}_id,\n".format(table)
	tableId = rowDict["{}_id".format(table)]
	vals = "\t'{}',\n".format(tableId)
	for h in headerLst:
		key = h["field"]
		val = rowDict[key]
		if len(str(val)) > 3:
			if str(val)[:3] == "999":
				val = "NULL"

		if h["type"] == "C" or h["type"] == "DATE":
			val = val.replace("'", "''")
			val = "'{}'".format(val)
		
		keys += "\t" + key.lower() + ",\n"
		vals += "\t" + str(val) + ",\n"

	keys += "\twrite_dt"
	vals += "\tnow()"

	insert = "INSERT INTO {} (\n{}\n) VALUES(\n{}\n);".format(table, keys, vals);
	return psqlExecute(insert)


def getDbColumns(schema, table):
	colData = {}

	query = "SELECT column_name, data_type " 
	query += "FROM information_schema.columns "
	query += "WHERE table_schema = '{}' ".format(schema)
  	query += "AND table_name   = '{}'".format(table)

	res = psqlSelect(query)

	for r in res:
		colData[r[0]] = r[1]

	return colData
		
def insertCsvRowData(table, headerLst, rowDict):
	keys = ""
	tableId = rowDict["{}_id".format(table)]
	vals = ""
	for key in headerLst:
		if key != "write_dt":
			val = rowDict[key]
			if len(str(val)) > 3:
				if str(val)[:3] == "999":
					val = "NULL"

			if val.replace(" ", "")  == "":
				val = "NULL"

			elif headerLst[key][:3] == "cha":
				val = val.replace("'", "''")
				val = "'{}'".format(val)

			keys += "\t" + key.lower() + ",\n"
			vals += "\t" + str(val) + ",\n"

	keys += "\twrite_dt"
	vals += "\tnow()"

	insert = "INSERT INTO {} (\n{}\n) VALUES(\n{}\n);".format(table, keys, vals);
	return psqlExecute(insert)

def getDataType(h):
	if h["type"] == "DATE":
		return "DATE"

	if h["type"] == "C":
		return "VARCHAR({})".format(int(h["size"]) * 2)	
		#return "VARCHAR(200)"
	
	elif h["type"] == "N" and h["prec"] == "0":
		return "INT"

	elif h["type"] == "N" and h["prec"] > "0":
		return "FLOAT"

	elif h["type"] == "GEOM":
		return "GEOMETRY"

def createTable(table, headerLst):
	create = "CREATE TABLE IF NOT EXISTS {} (\n\t{}_id VARCHAR(30),\n".format(table, table)

	for h in headerLst:
		field = h["field"].lower()
		dataType = getDataType(h)

		create += "\t{} {},\n".format(field, dataType)


	create += "\twrite_dt TIMESTAMP,\n"
	create += "\tPRIMARY KEY ({}_id)\n);".format(table)


	psqlExecute(create)

def fixHeaderLst(hLst, tLst):
	term = len(hLst)
	i = 0
	while i < term:
		h = hLst[i]
		for t in tLst:
			if h["field"].lower() ==  t:
				del hLst[i]
				i -= 1
				term = len(hLst)
		i +=1

def getAccidentHeaders(f):
	p = f["dbfDir"]

	dbfFile = dbf.Dbf("{}/{}".format(p, "accident.dbf"))

	headerLst = getDbfHeaderLst(dbfFile)
	l = []
	for h in headerLst:
		l.append(h["field"])

	hLst.append(l)

def parsePersonDbf(hFields, p, yr):
	print "\tparsing {}/person.dbf".format(p)

	f = "person.dbf"
	t = 0
	g = 0
	dbfFile = dbf.Dbf("{}/{}".format(p, f))

	headerLst = getDbfHeaderLst(hFields, dbfFile)

	headerLst.append({
		"field": "accident_id",
		"type": "C",
		"size": "30",
		"prec": "0"	
	}) 

	headerLst.append({
		"field": "vehicle_id",
		"type": "C",
		"size": "30",
		"prec": "0"	
	}) 


	table = f.lower().replace(".dbf", "")
	createTable(table, headerLst)

	for rec in dbfFile:
		data = rec.asDict()
		aid = "{}-{}".format(yr, int(data["ST_CASE"]))
		vid = "{}-{}".format(aid, int(data["VEH_NO"]))
		pid = "{}-{}".format(vid, int(data["PER_NO"]))

		data["{}_id".format(table)] = pid
		data["{}_id".format("accident")] = aid
		data["{}_id".format("vehicle")] = vid

		if STATE != None:
			if data["STATE"] == STATE:
				t += 1
				g += insertDbfRowData(table, headerLst, data)

		else:
				t += 1
				g += insertDbfRowData(table, headerLst, data)

	print "\t\t{} of {} written for {}".format(g, t, table)	

def parseVehicleDbf(hFields, p, yr):
	print "\tparsing {}/vehicle.dbf".format(p)

	f = "vehicle.dbf"
	t = 0
	g = 0
	dbfFile = dbf.Dbf("{}/{}".format(p, f))

	headerLst = getDbfHeaderLst(hFields, dbfFile)

	headerLst.append({
		"field": "accident_id",
		"type": "C",
		"size": "30",
		"prec": "0"	
	}) 

	table = f.lower().replace(".dbf", "")
	createTable(table, headerLst)

	for rec in dbfFile:
		data = rec.asDict()
		aid = "{}-{}".format(yr, int(data["ST_CASE"]))
		vid = "{}-{}".format(aid, int(data["VEH_NO"]))
		data["{}_id".format(table)] = vid

		data["{}_id".format("accident")] = aid

		if STATE != None:
			if data["STATE"] == STATE:
				t += 1
				g += insertDbfRowData(table, headerLst, data)

		else:
				t += 1
				g += insertDbfRowData(table, headerLst, data)

	print "\t\t{} of {} written for {}".format(g, t, table)	

def parsePersonCsv(hFields, p, yr):
	print "\tparsing {}/person.csv".format(p)

	f = "person.csv"
	t = 0
	g = 0
	csvFile = "{}/{}".format(p, f)

	table = f.lower().replace(".csv", "")
	
	headers = getDbColumns("public", table)

	headers["accident_id"] = "character varying"
	headers["vehicle_id"] = "charactor varying"
	
	csvData = getCsvData(hFields, csvFile)

	for i in range(0, len(csvData)):
		aid = "{}-{}".format(yr, int(csvData[i]["ST_CASE".lower()]))
		vid = "{}-{}".format(aid, int(csvData[i]["VEH_NO".lower()]))
		pid = "{}-{}".format(vid, int(csvData[i]["PER_NO".lower()]))

		csvData[i]["{}_id".format(table)] = pid
		csvData[i]["{}_id".format("accident")] = aid
		csvData[i]["{}_id".format("vehicle")] = vid

		if STATE != None:
			if data["STATE"] == STATE:
				t += 1
				g += insertCsvRowData(table, headers, csvData[i])

		else:
				t += 1
				g += insertCsvRowData(table, headers, csvData[i])


	print "\t\t{} of {} written for {}".format(g, t, table)	


def parseAccidentCsv(hFields, p, yr):
	print "\tparsing {}/accident.csv".format(p)

	f = "accident.csv"
	t = 0
	g = 0

	csvFile = "{}/{}".format(p, f)

	table = f.lower().replace(".csv", "")
	
	headers = getDbColumns("public", table)

	headers["geom"] = "GEOM"
	
	csvData = getCsvData(hFields, csvFile)
	
	for i in range(0, len(csvData)):
		lat = csvData[i]["LATITUDE".lower()]
		lng = csvData[i]["LONGITUD".lower()]
		srid = 4326
		csvData[i]["geom"] = "ST_GeomFromText('POINT({} {})', {})".format(lng, lat, srid)
		aid = "{}-{}".format(yr, int(csvData[i]["ST_CASE".lower()]))
		csvData[i]["{}_id".format(table)] = aid
	
		if STATE != None:
			if data["STATE"] == STATE:
				t += 1
				g += insertCsvRowData(table, headers, csvData[i])

		else:
				t += 1
				g += insertCsvRowData(table, headers, csvData[i])

	print "\t\t{} of {} written for {}".format(g, t, table)	
	
	


def parseAccidentDbf(hFields, p, yr):
	print "\tparsing {}/accident.dbf".format(p)

	f = "accident.dbf"
	t = 0
	g = 0
	dbfFile = dbf.Dbf("{}/{}".format(p, f))
		
	headerLst = getDbfHeaderLst(hFields, dbfFile)
	'''	
	headerLst.append({
		"field": "accident_date",
		"type": "DATE",
		"size": "0",
		"prec": "0"	
	})
	'''	
	headerLst.append({
		"field": "geom",
		"type": "GEOM",
		"size": "0",
		"prec": "0"	
	}) 

	table = f.lower().replace(".dbf", "")
	createTable(table, headerLst)

	for rec in dbfFile:
		data = rec.asDict()
		lat = data["LATITUDE"]
		lng = data["LONGITUD"]
		yr = int(data["YEAR"])
		mo = int(data["MONTH"])
		dy = int(data["DAY"])
		hr = int(data["HOUR"])
		mi = int(data["MINUTE"])
		srid = 4326
		data["geom"] = "ST_GeomFromText('POINT({} {})', {})".format(lng, lat, srid)
		aid = "{}-{}".format(yr, int(data["ST_CASE"]))
		data["{}_id".format(table)] = aid
		data["accident_date"] = "{}-{}-{}".format(yr, mo, dy)

		if STATE != None:
			if data["STATE"] == STATE:
				t += 1
				g += insertDbfRowData(table, headerLst, data)

		else:
				t += 1
				g += insertDbfRowData(table, headerLst, data)

	
	print "\t\t{} of {} written for {}".format(g, t, table)	
	

def getFLst(start, end, savePath):
	fLst = []
	year = end
	while year >= start:
		if year >= 2015:
			tgt = "FARS{}NationalCSV.zip".format(year)
			ftpDir ="{}/National/".format(year)

		elif year >= 2013:
			tgt = "FARS{}NationalDBF.zip".format(year)
			ftpDir ="{}/National/".format(year)

		elif year >= 2012:
			tgt = "FARS{}.zip".format(year)
			ftpDir ="{}/National/DBF/".format(year)

		elif year >= 2008:
			tgt = "FARS{}.zip".format(year)
			ftpDir ="{}/DBF/".format(year)

		elif year >= 2006:
			tgt = "FARS{}.ZIP".format(year)
			ftpDir ="{}/DBF/".format(year)

		elif year >= 2001:
			tgt = "FARS{}.zip".format(year)
			ftpDir ="{}/DBF/".format(year)

		saveTo = "{}/{}.zip".format(savePath, year)
		dbfDir = "{}/{}".format(savePath, year)
		ftpBack = ""
		for i in range(0, ftpDir.count("/")):
			ftpBack += "../"
			
		fLst.append({
			"year": year,
			"tgt": tgt,
			"ftpDir": ftpDir,
			"ftpBack": ftpBack,
			"saveTo": saveTo,
			"dbfDir": dbfDir
			
		})
		
		year -= 1
	return fLst

def makeDir(path):
	try:
		os.mkdir(path)
	except:
		pass


def getHeaders(f, hDict):
	p = f["saveTo"].replace(".zip", "")
	dataType = f["dataType"]
	for d in os.listdir(p):
		k = d.replace(".{}".format(dataType), "")
		if dataType == "dbf":
			dbfFile = dbf.Dbf("{}/{}".format(p, d))
			hLst = getDbfHeaderLst2(dbfFile)

		if dataType == "csv":
			csvFile = "{}/{}".format(p, d)
			hLst = getCsvHeaderLst2(csvFile)

		try:
			for v in hDict[k]:
				if v not in hLst:
					hDict[k].remove(v)

		except:
			hDict[k] = hLst
	
	
low = 2001
high = 2017

#ftp = ftpConnect(ftpUrl, root)
makeDir(temp)

hDict = {}
fLst = getFLst(low, high, temp)

for i in range(0, len(fLst)):
	f = fLst[i]
	#fetchData(ftp, f)
	fLst[i]["dataType"] = unzipData(f)
	getHeaders(f, hDict)


for f in reversed(fLst):
	fDir = f["saveTo"].replace(".zip", "")
	yr = f["year"]
	print yr
	if f["dataType"] == "xdbf":
		print 'parse as dbf'
		parseAccidentDbf(hDict["accident"], fDir, yr)
		parsePersonDbf(hDict["person"], fDir, yr)
		'''
		parseVehicleDbf(hDict["accident"], fDir, yr)
		'''
	if f["dataType"] == "csv":
		#parseAccidentCsv(hDict["accident"], fDir, yr)
		parsePersonCsv(hDict["accident"], fDir, yr)


#ftp.close()
#shutil.rmtree(temp)

