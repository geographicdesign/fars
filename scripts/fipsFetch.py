import urllib
import xlrd
import psycopg2
import sys

reload(sys)
sys.setdefaultencoding("utf-8")

host = "localhost"
db = "fars"
user = "postgres"
pwd = "RooCoon051708"

conn_str = "host='{}' dbname='{}' user='{}' password='{}'".format(host, db, user, pwd)

conn = psycopg2.connect(conn_str)
conn.autocommit = True
cursor = conn.cursor()

fipsUrl = "https://www.gsa.gov/cdnstatic/FRPP%20GLC%20United%20States.xlsx"
temp = "../temp"

def psqlExecute(query):
	try:
		cursor.execute(query)
		return 1
	except:
		print query
		print
		print
		return 0


def fetchFipsXlsx(path):
	fipsUrl = "https://www.gsa.gov/cdnstatic/FRPP%20GLC%20United%20States.xlsx"
	urllib.urlretrieve(fipsUrl, path)

def parseXlsx(path):
	data = []
	book = xlrd.open_workbook(path)
	sheet = book.sheet_by_index(0)
	headers = sheet.row_values(1)
	for i in range(0, len(headers)):
		headers[i] = headers[i].lower().replace(" ", "_")
		
	for i in range(2, sheet.nrows):
		d = {}
		row = sheet.row_values(i)
		for i in range(0, len(headers)):
			key = headers[i].lower().replace(" ", "_")
			val = row[i]
			d[key] = val

		data.append(d)

	return data


def getDataTypes(data):
	dataTypes = {}

	for k in data[0]:
		dataTypes[k] = {
			"int": 0,
			"char": 0,
			"null": 0
		}

	for row in data:
		for d in row:
			if row[d] == "":
				dataTypes[d]["null"] += 1

			else:
				try:
					int(row[d])
					dataTypes[d]["int"] += 1
				except:
					dataTypes[d]["char"] += 1


	for d in dataTypes:
		if dataTypes[d]["null"] / (len(data) * 1.0) > .75:
			dataTypes[d] = "null"

		elif dataTypes[d]["int"] / (len(data) * 1.0) == 1:
			dataTypes[d] = "int"

		else:
			dataTypes[d] = "char"

	return dataTypes

def createTable(dataTypes):
	drop = "DROP TABLE IF EXISTS fips;"
	psqlExecute(drop)

	create = "CREATE TABLE fips (\n"
		
	for d in dataTypes:
		if dataTypes[d] == "int":
			val = d
			typ = "INT"
			create += "\t{} {},\n".format(val, typ)

		elif dataTypes[d] == "char":
			val = "{}".format(d.replace("'", "''"))
			typ = "VARCHAR(50)"
			create += "\t{} {},\n".format(val, typ)


	create = "{} write_dt TIMESTAMP\n);".format(create)
	
	psqlExecute(create)

def writeData(dataTypes, data):
	x = 0
	g = 0
	for row in data:
		keys = "(\n"
		vals = "(\n"
		for k in dataTypes:
			if dataTypes[k] == "int":
				keys += "\t{},\n".format(k)
				vals += "\t{},\n".format(row[k])

			elif dataTypes[k] == "char":
				keys += "\t{},\n".format(k)
				val = row[k].replace("'", "''")[:49]
				vals += "\t'{}',\n ".format(val)
	
		keys += "\twrite_dt\n)"
		vals += "\tnow()\n)"

		insert = "INSERT INTO fips {} VALUES {};".format(keys, vals)	

		x += 1	
		g += psqlExecute(insert)

	print "{} of {} written".format(g, x)

	

			
	
		



saveTo = "{}/fips.xlsx".format(temp)
#fetchFipsXlsx(saveTo)
data = parseXlsx(saveTo)
dataTypes = getDataTypes(data)
createTable(dataTypes)
writeData(dataTypes, data)

